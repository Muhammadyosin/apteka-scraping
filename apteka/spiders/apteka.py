import re
import scrapy
import time


class AptekaSpiderSpider(scrapy.Spider):
    name = "apteka"
    allowed_domains = ["apteka-ot-sklada.ru"]
    start_urls = [
        "https://apteka-ot-sklada.ru/catalog/sredstva-gigieny%2Fuhod-za-polostyu-rta%2Fzubnye-niti_-ershiki?start=0",
        "https://apteka-ot-sklada.ru/catalog/izdeliya-meditsinskogo-naznacheniya/aptechki",
        "https://apteka-ot-sklada.ru/catalog/meditsinskaya-tekhnika/drugaya-medtekhnika",
        "https://apteka-ot-sklada.ru/catalog/meditsinskaya-tekhnika/gigiena-nosa",
        "https://apteka-ot-sklada.ru/catalog/meditsinskaya-tekhnika/glyukometry"
        ]
    

    def parse(self, response):
        product_links = response.css('.goods-card__link::attr(href)').getall()
        for product_link in product_links:
            yield response.follow(product_link, callback=self.parse_product)
        next_page = response.css('li.ui-pagination__item_next a.ui-pagination__link_direction::attr(href)').get()
        if next_page:   
            time.sleep(3)
            yield response.follow(next_page, callback=self.parse)

#    def parse_2(self, response):
#        time.sleep(2)
#        product_links = response.css('.goods-card__link::attr(href)').getall()
#        for product_link in product_links:
#            yield response.follow(product_link, callback=self.parse_product)


    def parse_product(self, response):
        product_name = response.css('h1.text.text_size_display-1.text_weight_bold span[itemprop="name"]::text').get()
        price_string = response.css('div.goods-offer-panel__price span.goods-offer-panel__cost::text').get()
        price = re.findall(r'\d+\.\d+', str(price_string))
        extrated_price = float(price[0]) if price else None
        description = response.css('div[itemprop="description"] ::text').getall()
        description_text = ' '.join(description).strip()

        yield {
            'product_name': product_name,
            'price': extrated_price,
            'in_stock': True if extrated_price else False,
            'description': description_text,
            'product_link': response.url
        }